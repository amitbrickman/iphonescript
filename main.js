const puppeteer = require("puppeteer");
// const fetch = require("node-fetch");
const url =
  "https://ksp.co.il/index.php?select=.272..573.&kg=&list=1&sort=1&glist=0&uin=0&txt_search=iphone+12+pro&buy=&minprice=0&maxprice=0&intersect=&rintersect=&store_real=";
const whatsappBotApiUrl =
  "http://api.callmebot.com/whatsapp.php?source=web&phone=+972503377231&apikey=602044&text=Iphone%20is%20in%20stock";
// const searchedText = "אייפון Apple iPhone 12 Pro Max 128GB צבע אפור";
const searchedText = "אייפון Apple iPhone 12 Pro Max 256GB";

const express = require("express");
const { response } = require("express");
const app = express();
const port = 3000;

app.get("/", async (req, res) => {
  if (!url) {
    res.status(400).send("Bad request: 'url' param is missing!");
    return;
  }

  try {
    setInterval(searchInSite, 70000);
    res.status(200).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

const searchInSite = async () => {
  console.log("Start searching...");
  const html = await getPageHTML(url);

  if (isExistInHtml(html, searchedText)) {
    try {
      const res = await fetch(whatsappBotApiUrl);
      console.log(res);
    } catch (err) {
      console.log(err);
    }
    console.log("Found!");
  } else {
    console.log("Not found");
  }
};

const isExistInHtml = (htmlCode, textToSearch) => {
  return htmlCode.includes(textToSearch);
};

const getPageHTML = async (pageUrl) => {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();

  await page.goto(pageUrl);

  const pageHTML = await page.evaluate(
    "new XMLSerializer().serializeToString(document.doctype) + document.documentElement.outerHTML"
  );

  await browser.close();

  return pageHTML;
};

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
